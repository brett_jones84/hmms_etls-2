@echo off
pushd "%~dp0"

REM - LOCAL
REM sqlcmd.exe -S tcp:vmsql2012,1433 -imain.sql -vMAIN_DB="HMMS" -vSTAGING_DB="HMMS_STAGING" -vMSCRM="CSC_MSCRM" > results.log

REM DEV
sqlcmd.exe -S tcp:wsq01438,55001 -imain.sql -vMAIN_DB="HMMS" -vSTAGING_DB="HMMS_STAGING" -vMSCRM="CSC_MSCRM"> results.log

REM UAT
REM sqlcmd.exe -S tcp:wsq01443,55001 -imain.sql -vMAIN_DB="HMMS" -vSTAGING_DB="HMMS_STAGING" -vMSCRM="CSC_MSCRM"> results.log



popd
PAUSE