USE [$(MAIN_DB)]
GO

IF OBJECT_ID('vdot_vw_hmms_WorkOrder_Linear_Default') IS NOT NULL
	DROP VIEW  [dbo].[vdot_vw_hmms_WorkOrder_Linear_Default]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE VIEW [dbo].[vdot_vw_hmms_WorkOrder_Linear_Default] AS

SELECT wo.ID
	, wo.WorkOrderID 	
	, CASE WHEN wo_lmf.[ID] IS NOT NULL THEN 'Linear'
		WHEN wo_mf.[ID] IS NOT NULL THEN 'Default'
		ELSE 'Other'
		END AS FormType
	,wo_mf.[Agency Use 1] AS [Agency Use 1]
	,wo_mf.[Agency Use 2] AS [Agency Use 2]
	,COALESCE (wo_lmf.[AHQ Name (Base Map)],wo_mf.[AHQ Name (Base Map)]) AS [AHQ Name (Base Map)]
	,COALESCE (wo_lmf.[Assigned/Pending],wo_mf.[Assigned/Pending]) AS [Assigned/Pending]
	,COALESCE (wo_lmf.[AU1 (If applicable)],wo_mf.[AU1 (If applicable)]) AS [AU1 (If applicable)]
	,COALESCE (wo_lmf.[AU2 (Route Number)],wo_mf.[AU2 (Route)]) AS [AU2 (Route)]
	,COALESCE (wo_lmf.[Cancellation Reason],wo_mf.[Cancellation Reason]) AS [Cancellation Reason]
	,COALESCE (wo_lmf.[Charge To Department],wo_mf.[Charge To Department]) AS [Charge To Department]
	,COALESCE (wo_lmf.[City Code (FIPS Code)],wo_mf.[City Code (FIPS Code)]) AS [City Code (FIPS Code)]
	,COALESCE (wo_lmf.[City Name (Base Map)],wo_mf.[City Name (Base Map)]) AS [City Name (Base Map)]
	,COALESCE (wo_lmf.[Closed Status Reason (To Customer)],wo_mf.[Closed Status Reason (To Customer)]) AS [Closed Status Reason (To Customer)]
	,COALESCE (wo_lmf.[County Code (FIPS Code)],wo_mf.[County Code (FIPS Code)]) AS [County Code (FIPS Code)]
	,COALESCE (wo_lmf.[County Name (Base Map)],wo_mf.[County Name (Base Map)]) AS [County Name (Base Map)]	
	,COALESCE (wo_lmf.[Department ID],wo_mf.[Department ID]) AS [Department ID]
	,COALESCE (wo_lmf.[Disaster],wo_mf.[Disaster]) AS [Disaster]
	,COALESCE (wo_lmf.[District Name (Base Map)],wo_mf.[District Name (Base Map)]) AS [District Name (Base Map)]
	,COALESCE (wo_lmf.[Federal Structure ID],wo_mf.[Federal Structure ID]) AS [Federal Structure ID]
	,wo_lmf.[From Mile Point] AS [From Mile Point]
	,COALESCE (wo_lmf.[Mile Marker (Base Map)],wo_mf.[Mile Marker (Base Map)]) AS [Mile Marker (Base Map)]
	,COALESCE (wo_lmf.[Open Status Reason (To Customer)],wo_mf.[Open Status Reason (To Customer)]) AS [Open Status Reason (To Customer)]
	,COALESCE (wo_lmf.[Quantity],wo_mf.[Quantity]) AS [Quantity]
	,COALESCE (wo_lmf.[Residency Name (Base Map)],wo_mf.[Residency Name (Base Map)]) AS [Residency Name (Base Map)]
	,COALESCE (wo_lmf.[Response to Customer (Optional)],wo_mf.[Response to Customer (Optional)]) AS [Response to Customer (Optional)]
	,wo_lmf.[Route Name (Base Map)] AS [Route Name (Base Map)]
	,COALESCE (wo_lmf.[State Structure No.],wo_mf.[State Structure No.]) AS [State Structure No.]
	,wo_mf.[Structure] AS [Structure]
	,COALESCE (wo_lmf.[System],wo_mf.[System]) AS [System]
	,wo_mf.[System (Base Map)] AS [System (Base Map)]
	,wo_lmf.[To Mile Point] AS [To Mile Point]
	,COALESCE (wo_lmf.[Town Code (FIPS Code)],wo_mf.[Town Code (FIPS Code)]) AS [Town Code (FIPS Code)]
	,COALESCE (wo_lmf.[Town Name (Base Map)],wo_mf.[Town Name (Base Map)]) AS [Town Name (Base Map)]
	,wo_mf.[Town Name (FIPS Code)] AS [Town Name (FIPS Code)]
	,COALESCE (wo_lmf.[UOM],wo_mf.[UOM]) AS [UOM]
	,COALESCE (wo_lmf.[UPC Number],wo_mf.[UPC Number]) AS [UPC Number]
	,COALESCE (wo_lmf.[VDOT City Code (Base Map)],wo_mf.[VDOT City Code (Base Map)]) AS [VDOT City Code (Base Map)]
	,COALESCE (wo_lmf.[VDOT County Code (Base Map)],wo_mf.[VDOT County Code (Base Map)]) AS [VDOT County Code (Base Map)]
	,COALESCE (wo_lmf.[VDOT Town Code (Base Map)],wo_mf.[VDOT Town Code (Base Map)]) AS [VDOT Town Code (Base Map)]
FROM reports.WorkOrders as wo
left join reports.WorkOrderForm_MaintenanceLinearWoForm wo_lmf on wo.ID = wo_lmf.ID
left join reports.WorkOrderForm_MaintenanceWorkOrderForm wo_mf on wo.ID = wo_mf.ID




GO
