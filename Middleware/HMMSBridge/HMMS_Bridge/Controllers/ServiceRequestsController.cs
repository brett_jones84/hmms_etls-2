﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using HMMS_DAL;
using CSCOperations;

namespace HMMS_Bridge.Controllers
{
    ///ServiceRequestsController
    /// <summary>
    /// Handles all calls to interact with Service Requests
    ///     Update Service Request with a VueWorks ID.
    ///     Update Service Request with new status - if update status is the same as existing status then the update is discarded.
    /// </summary>
    public class ServiceRequestsController : ApiController
    {

        private SCRIBEINTERNALEntities db = new SCRIBEINTERNALEntities();

        /// <summary>
        /// Returns all VDOT service requests - will always have data
        /// </summary>
        /// <returns>vdot_servicerequest_temp_all</returns>
        // GET: api/vdot_servicerequest_temp_all
        [Route("~/api/vdot_servicerequest_temp_all/")]
        public IQueryable<vdot_servicerequest_temp_all> Getvdot_servicerequest_temp_all()
        {
            return db.vdot_servicerequest_temp_all;
        }

        /// <summary>
        /// Gets all recent VDOT service requests.
        /// </summary>
        /// <returns></returns>
        // GET: api/vdot_servicerequest_v2
        [Route("~/api/vdot_servicerequest_v2/")]
        public IQueryable<vdot_servicerequest_v2> Getvdot_servicerequest_v2()
        {
            return db.vdot_servicerequest_v2;
        }

        // POST api/AddVueWorksIDToServiceRequest/1?VW_SRID=5
        /// <summary>
        /// Update a service request with a VueWork ID
        /// </summary>
        /// <param name="VDOT_SRID"></param>
        /// <param name="VW_SRID"></param>
        /// <returns></returns>
        //[Route("~/api/AddVueWorksIDToServiceRequest/{VDOT_SRID}/")]
        //[HttpPost]
        private HttpResponseMessage _AddVueWorksIDToServiceRequest(string VDOT_SRID, string VW_SRID)
        {
            CRUDOperations app = new CRUDOperations();
            if (app.initializationResult.status == HttpStatusCode.InternalServerError)
            {
                return Request.CreateResponse(app.initializationResult.status, app.initializationResult);
            }
            else
            {
                CRUDOperations_Result result = app.update_VDOT_AMSID(VDOT_SRID, VW_SRID);
                //result.status comes from CRUDOperations.
                return Request.CreateResponse(result.status, result);
            }

        }

        /// <summary>
        /// Async Method for Adding VueWorks ID to Service Request
        /// </summary>
        /// <param name="VDOT_SRID"></param>
        /// <param name="VW_SRID"></param>
        /// <returns></returns>
        [Route("~/api/AddVueWorksIDToServiceRequest/{VDOT_SRID}/")]
        [HttpPost]
        public async Task<HttpResponseMessage> AddVueWorksIDToServiceRequest(string VDOT_SRID, string VW_SRID)
        {
            return await Task.FromResult(_AddVueWorksIDToServiceRequest(VDOT_SRID, VW_SRID));
        }

        // POST api/UpdateServiceRequestStatus/{1}?VDOT_SR_Status={"CLOSED"}
        /// <summary>
        /// Update a service request with a VueWork ID
        /// </summary>
        /// <param name="VDOT_SRID"></param>
        /// <param name="VDOT_SR_Status"></param>
        /// <returns></returns>
        //[Route("~/api/UpdateServiceRequestStatus/{VDOT_SRID}/")]
        //[HttpPost]
        private HttpResponseMessage _UpdateServiceRequestStatus(string VDOT_SRID, string VDOT_SR_Status)
        {
            CRUDOperations app = new CRUDOperations();
            if (app.initializationResult.status == HttpStatusCode.InternalServerError)
            {
                return Request.CreateResponse(app.initializationResult.status, app.initializationResult);
            }
            else
            {
                CRUDOperations_Result result = app.update_VDOT_Status(VDOT_SRID, VDOT_SR_Status);
                //result.status comes from CRUDOperations.
                HttpResponseMessage newResponse = Request.CreateResponse(result.status, result);
                return newResponse;
            }


        }

        /// <summary>
        /// Async Method for updating Service Request Status
        /// </summary>
        /// <param name="VDOT_SRID"></param>
        /// <param name="VDOT_SR_Status"></param>
        /// <returns></returns>
        [Route("~/api/UpdateServiceRequestStatus/{VDOT_SRID}/")]
        [HttpPost]
        public async Task<HttpResponseMessage> UpdateServiceRequestStatus(string VDOT_SRID, string VDOT_SR_Status)
        {
            return await Task.FromResult(_UpdateServiceRequestStatus(VDOT_SRID, VDOT_SR_Status));
        }

    }

}