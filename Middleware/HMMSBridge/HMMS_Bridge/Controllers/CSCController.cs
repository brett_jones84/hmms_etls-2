﻿using System;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using HMMS_Bridge.Models;
using CSCOperations;
using HMMS_DAL;


namespace HMMS_Bridge.Controllers
{
    /// <summary>
    /// Exposes interactions for CSC
    /// </summary>
    public class CSCController : ApiController
    {
        private HMMSEntities db = new HMMSEntities();



        /// <summary>
        /// Testing payload outputs from IIB
        /// </summary>
        /// <param name="CSC_SR_ID "></param>
        /// <param name="SR_WorkArea_ID"></param>
        /// <returns>HttpResponseMessage</returns>
        /// POST api/CSC/servicerequests/testpayload/
        [Route("~/api/CSC/servicerequests/testpayload")]
        [HttpPost]
        public async Task<string> testpayload()
        {
            C_test_mhk_delete newEntry = new C_test_mhk_delete();
            newEntry.ID = Guid.NewGuid();
            string result = await Request.Content.ReadAsStringAsync();
            newEntry.payload = result;

            db.C_test_mhk_delete.Add(newEntry);
            db.SaveChanges();

            HttpResponseMessage newResponse = Request.CreateResponse(HttpStatusCode.OK, result);
            return result;
        }





        /// <summary>
        /// Update a CSC service request with a new work area
        /// </summary>
        /// <param name="CSC_SR_ID "></param>
        /// <param name="SR_WorkArea_ID"></param>
        /// <returns>HttpResponseMessage</returns>
        private HttpResponseMessage _UpdateServiceRequestWorkArea(string CSC_SR_ID, string SR_WorkArea_ID)
        {
            CRUDOperations app = new CRUDOperations();
            if (app.initializationResult.status == HttpStatusCode.InternalServerError)
            {
                return Request.CreateResponse(app.initializationResult.status, app.initializationResult);
            }
            else
            {
                CRUDOperations_Result result = app.update_VDOT_WorkArea(CSC_SR_ID, SR_WorkArea_ID);
                //result.status comes from CRUDOperations.
                HttpResponseMessage newResponse = Request.CreateResponse(result.status, result);
                return newResponse;
            }


        }

        /// <summary>
        /// Update CSC Service Request Work Area
        /// </summary>
        /// <param name="CSC_SR_ID "></param>
        /// <param name="SR_WorkArea_ID"></param>
        /// <returns>HttpResponseMessage</returns>
        /// POST api/CSC/servicerequests/updateWorkArea/{CSC_SR_ID}?SR_WorkArea_ID={SR_WorkArea_ID}
        [Route("~/api/CSC/servicerequests/updateWorkArea/{CSC_SR_ID}/")]
        [HttpPost]
        public async Task<HttpResponseMessage> UpdateServiceRequestWorkArea(string CSC_SR_ID, string SR_WorkArea_ID)
        {
            return await Task.FromResult(_UpdateServiceRequestWorkArea(CSC_SR_ID, SR_WorkArea_ID));
        }



        /// <summary>
        /// Update a CSC service request with a HMMS work order ID
        /// </summary>
        /// <param name="CSC_SR_ID "></param>
        /// <param name="HMMS_WO_ID"></param>
        /// <returns>HttpResponseMessage</returns>
        private HttpResponseMessage _UpdateServiceRequestWorkOrderID(string CSC_SR_ID, string HMMS_WO_ID)
        {
            CRUDOperations app = new CRUDOperations();
            if (app.initializationResult.status == HttpStatusCode.InternalServerError)
            {
                return Request.CreateResponse(app.initializationResult.status, app.initializationResult);
            }
            else
            {
                CRUDOperations_Result result = app.update_VDOT_WorkOrder_ID(CSC_SR_ID, HMMS_WO_ID);
                //result.status comes from CRUDOperations.
                HttpResponseMessage newResponse = Request.CreateResponse(result.status, result);
                return newResponse;
            }


        }

        /// <summary>
        /// Update CSC Service Request Work Area
        /// </summary>
        /// <param name="CSC_SR_ID "></param>
        /// <param name="HMMS_WO_ID"></param>
        /// <returns>HttpResponseMessage</returns>
        /// POST api/CSC/servicerequests/updateWorkOrderID/{CSC_SR_ID}?HMMS_WO_ID={HMMS_WO_ID}
        [Route("~/api/CSC/servicerequests/updateWorkOrderID/{CSC_SR_ID}/")]
        [HttpPost]
        public async Task<HttpResponseMessage> UpdateServiceRequestWorkOrderID(string CSC_SR_ID, string HMMS_WO_ID)
        {
            return await Task.FromResult(_UpdateServiceRequestWorkOrderID(CSC_SR_ID, HMMS_WO_ID));
        }











        /// <summary>
        /// Update a CSC service request with a new status
        /// </summary>
        /// <param name="CSC_SR_ID "></param>
        /// <param name="HMMS_SR_Status"></param>
        /// <returns>HttpResponseMessage</returns>
        private HttpResponseMessage _UpdateServiceRequestStatus(string CSC_SR_ID, string HMMS_SR_Status)
        {
            CRUDOperations app = new CRUDOperations();
            if (app.initializationResult.status == HttpStatusCode.InternalServerError)
            {
                return Request.CreateResponse(app.initializationResult.status, app.initializationResult);
            }
            else
            {
                CRUDOperations_Result result = app.update_VDOT_Status(CSC_SR_ID, HMMS_SR_Status);
                //result.status comes from CRUDOperations.
                HttpResponseMessage newResponse = Request.CreateResponse(result.status, result);
                return newResponse;
            }


        }

        /// <summary>
        /// Update CSC Service Request Status
        /// </summary>
        /// <param name="CSC_SR_ID "></param>
        /// <param name="CSC_SR_Status"></param>
        /// <returns>HttpResponseMessage</returns>
        /// POST api/CSC/servicerequests_updatestatus/{CSC_SR_ID}?HMMS_SR_Status={HMMS_SR_Status}
        [Route("~/api/CSC/servicerequests/updatestatus/{CSC_SR_ID}/")]
        [HttpPost]
        public async Task<HttpResponseMessage> UpdateServiceRequestStatus(string CSC_SR_ID, string CSC_SR_Status)
        {
            return await Task.FromResult(_UpdateServiceRequestStatus(CSC_SR_ID, CSC_SR_Status));
        }

        /// <summary>
        /// Update a CSC service request with a HMMS ID
        /// </summary>
        /// <param name="CSC_SR_ID "></param>
        /// <param name="HMMS_SR_ID "></param>
        /// <returns> HttpResponseMessage </returns>
        private HttpResponseMessage _updatehmmsid(string CSC_SR_ID, string HMMS_SR_ID)
        {
            CRUDOperations app = new CRUDOperations();
            if (app.initializationResult.status == HttpStatusCode.InternalServerError)
            {
                return Request.CreateResponse(app.initializationResult.status, app.initializationResult);
            }
            else
            {
                CRUDOperations_Result result = app.update_VDOT_AMSID(CSC_SR_ID, HMMS_SR_ID);
                //result.status comes from CRUDOperations.
                return Request.CreateResponse(result.status, result);
            }

        }

        /// <summary>
        /// Update a CSC service request with a HMMS ID
        /// </summary>
        /// <param name="CSC_SR_ID "></param>
        /// <param name="HMMS_SR_ID "></param>
        /// <returns>HttpResponseMessage</returns>
        /// POST api/CSC/servicerequests_updatehmmsid/{CSC_SR_ID}/
        [Route("~/api/CSC/servicerequests/updatehmmsid/{CSC_SR_ID}/")]
        [HttpPost]
        public async Task<HttpResponseMessage> updatehmmsid(string CSC_SR_ID, string HMMS_SR_ID)
        {
            return await Task.FromResult(_updatehmmsid(CSC_SR_ID, HMMS_SR_ID));
        }


        /// <summary>
        /// Gets all recent service requests from CSC.
        /// </summary>
        /// <returns>vdot_vw_csc_new_SRs</returns>
        // GET: api/CSC/
        [Route("~/api/CSC/servicerequests")]
        public IQueryable<vdot_vw_csc_new_SRs> Getservicerequests()
        {
            return db.vdot_vw_csc_new_SRs;
        }


        /// <summary>
        /// Gets all recent service requests from CSC.
        /// </summary>
        /// <returns>vdot_vw_csc_new_SRs</returns>
        // GET: api/CSC/
        [Route("~/api/CSC/servicerequests/noWOIds")]
        public IQueryable<vdot_vw_csc_SRs_No_WOID> Getservicerequestsnowoids()
        {
            return db.vdot_vw_csc_SRs_No_WOID;
        }



        /// <summary>
        /// Gets pending service requests cancels from CSC.
        /// </summary>
        /// <returns>vdot_vw_csc_pendingCancelSRs</returns>
        // GET: api/CSC/servicerequests_pendingcancels/
        [Route("~/api/CSC/servicerequests/pendingcancels/")]
        public IQueryable<vdot_vw_csc_pendingCancelSRs> Getpendingcancels()
        {
            return db.vdot_vw_csc_pendingCancelSRs;
        }






        /// <summary>
        /// Returns all CSC comments from the CSC database
        /// </summary>
        /// <returns>vdot_vw_csc_comments</returns>
        [Route("~/api/CSC/servicerequests/Comments")]
        [HttpGet]
        public IQueryable<vdot_vw_csc_comments> CSCComments()
        {
            //TODO DELETE THIS NEXT TWO LINEs
            //CRUDOperations app = new CRUDOperations();
            //app.displayEntityAttributes();

            //app.update_VDOT_WorkArea("116122", "70309|3");

            return db.vdot_vw_csc_comments;
        }

        // POST: api/Comments
        /// <summary>
        /// Adds a Comment to a service account based on Service Account ID.
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>

        //[Route("~/api/Comment")]
        //[HttpPost]
        private HttpResponseMessage _PostComment(Comment comment)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (comment.useDBForCompare)
            {
                using (var context = new HMMS_DAL.HMMSEntities())
                {
                    var vdot_vw_csc_commentsResult =
                        context.vdot_vw_csc_comments.Where(
                            n => (n.CSC_SR_ID == comment.CSC_SR_ID && n.NoteText == comment.CommentText)).ToList();

                    //if there are any matching comments do not insert.
                    if (vdot_vw_csc_commentsResult.Count == 0)
                    {
                        return updateComment(comment);
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                            "A comment was not inserted into Service Request{0} because the comment text already exists",
                            comment.CSC_SR_ID);
                    }
                }
            }
            else
            {
                return updateComment(comment);
            }

        }

        /// <summary>
        /// Add a comment to CSC service request.
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        [Route("~/api/CSC/servicerequests/Comment")]
        [HttpPost]
        public async Task<HttpResponseMessage> PostCommentAsync(Comment comment)
        {
            return await Task.FromResult(_PostComment(comment));
        }

        private HttpResponseMessage updateComment(Comment comment)
        {

            CRUDOperations app = new CRUDOperations();
            if (app.initializationResult.status == HttpStatusCode.InternalServerError)
            {
                return Request.CreateResponse(app.initializationResult.status, app.initializationResult);
            }
            else
            {
                CRUDOperations_Result result = app.add_Comment(comment.CSC_SR_ID, comment.CommentText,
                    comment.TimeStamp, comment.CommentSubject, comment.useDBForCompare);
                return Request.CreateResponse(result.status, result);

            }


        }

    }
}
