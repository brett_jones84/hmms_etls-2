﻿using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using HMMS_Bridge.Models;
using CSCOperations;

namespace HMMS_Bridge.Controllers
{
    /// <summary>
    /// Handles all calls to interact with Comments
    /// Add Comment based on VDOT Service Request ID, If a comment exists for the service record that has an identical timestamp the insert is discarded.
    /// </summary>
    public class CommentsController : ApiController
    {


        // POST: api/Comments
        /// <summary>
        /// Adds a Comment to a service account based on Service Account ID.
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>

        //[Route("~/api/Comment")]
        //[HttpPost]
        private HttpResponseMessage _PostComment(Comment comment)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (comment.useDBForCompare)
            {
                using (var context = new HMMS_DAL.SCRIBEINTERNALEntities())
                {
                    var vdot_vw_csc_commentsResult =
                        context.vdot_vw_csc_comments.Where(
                            n => (n.vdot_srid == comment.VDOT_SRID && n.NoteText == comment.CommentText)).ToList();

                    //if there are any matching comments do not insert.
                    if (vdot_vw_csc_commentsResult.Count == 0)
                    {
                        return updateComment(comment);
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK,
                            "A comment was not inserted into Service Request{0} because the comment text already exists",
                            comment.VDOT_SRID);
                    }
                }
            }
            else
            {
                return updateComment(comment);
            }
            
        }

        /// <summary>
        /// Asnc method for adding comment to a service Request.
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        [Route("~/api/Comment")]
        [HttpPost]
        public async Task<HttpResponseMessage> PostCommentAsync(Comment comment)
        {
            return await Task.FromResult(_PostComment(comment));
        }

        private HttpResponseMessage updateComment(Comment comment)
        {

            CRUDOperations app = new CRUDOperations();
            if (app.initializationResult.status == HttpStatusCode.InternalServerError)
            {
                return Request.CreateResponse(app.initializationResult.status, app.initializationResult);
            }
            else
            {
                CRUDOperations_Result result = app.add_Comment(comment.VDOT_SRID, comment.CommentText,
                    comment.TimeStamp, comment.useDBForCompare);
                return Request.CreateResponse(result.status, result);

            }


        }


    }
}
